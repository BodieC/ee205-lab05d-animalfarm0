////////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// Usage:  To store,update, print , and delete cats into a database
///
/// Result:
///   Print out the energy unit conversion
///
/// Compilation:
///   $ make animalFarm
///
/// @file animalFarm0.c
///
/// @author Bodie Collins < bodie@hawaii.edu>
/// @date  20 FEB 2022
///////////////////////////////////////////////////////////////////////////////

#include "catDatabse.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "addCats.h"
#include "reportCats.h"
#include "deleteCats.h"
#include "updateCats.h"


int main()
{
printf("Starting Animal Farm 0\n");

addCats( "Loki", MALE, PERSIAN, true, 8.5 ) ;
addCats( "Milo", MALE, MANX, true, 30.0 ) ;
addCats( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
addCats( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
addCats( "Bell", FEMALE, MANX, true, 12.2 ) ;
addCats( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;
//printAllCats();
//printCat(0);
//printCat(2);
//printCat(6);
//printCat(0);
//printCat(35);
//findCat ("Milo");
//findCat ("Bell");
//findCat ("Dog");
//findCat("Loki");
//printCat(2);
//updateCatName(2 , "Bell");
//printCat(2);
//printf("Done with Animal Farm 0\n");

//printCat(4);
//fixCat(4);
//printCat(4);

//printCat(6);
//updateCatWeight(6,0);
//printCat(6);

//printAllCats();
//deleteAllCats();
//printAllCats();

printAllCats();
deleteCat(3);
printAllCats();

}

