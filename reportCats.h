
/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// @file reportCats.h
/// @version 1.0
///
/// @author Bodie Collins <bodie@hawaii.edu>
/// @date 22 Feb 2022
/////////////////////////////////////////////////////////////////////////////
#pragma once

extern int printAllCats();
extern int printCat(size_t catnuM);
extern int findCat(char* naME);


