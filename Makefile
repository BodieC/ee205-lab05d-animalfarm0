###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### animalFarm0
###
### @author Bodie Collins <bodie@hawaii.edu>
### @date 22 Feb 2022
###
###############################################################################
CC = gcc
CFLAGS = -g -Wall -Wextra

TARGET =animalFarm0

all: $(TARGET)

addCats.o: addCats.c addCats.h catDatabse.h 
	$(CC) $(CFLAGS) -c addCats.c

reportCats.o: reportCats.c reportCats.h catDatabse.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h catDatabse.h
	$(CC) $(CFLAGS) -c updateCats.c

deleteCats.o: deleteCats.c deleteCats.h catDatabse.h
	$(CC) $(CFLAGS) -c deleteCats.c

animalFarm0.o: animalFarm0.c addCats.h catDatabse.h reportCats.h deleteCats.h updateCats.h
	$(CC) $(CFLAGS) -c animalFarm0.c

animalFarm0: animalFarm0.o addCats.o reportCats.o deleteCats.h updateCats.h
	$(CC) $(CFLAGS) -o $(TARGET) animalFarm0.o addCats.o reportCats.o deleteCats.o updateCats.o

tmp: tmp.c 
	$(CC) $(CFLAGS) -o tmp tmp.c


clean:
	.rm -f $(TARGET) *.o

