////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// Usage:  functions to delete cats from the database
///
/// @file deleteCat.c
///
/// @author Bodie Collins < bodie@hawaii.edu>
/// @date  22 Feb 2022
///////////////////////////////////////////////////////////////////////

#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "catDatabse.h"
#include "deleteCats.h"

//FUNCTION TO DELETE ALL CATS
int deleteAllCats()
{
   size_t foo=numofCats;

   while(foo>0)
   {
   nameArray[foo]="";
   genderArray[foo]=0;
   breedArray[foo]=0;
   fixedArray[foo]=0;
   weightArray[foo]=0;
   foo=foo-1;
   }
}

//DELETE ONE CAT
int deleteCat(size_t cATNUM)
{
   nameArray[cATNUM]="";
   genderArray[cATNUM]=0;
   breedArray[cATNUM]=0;
   fixedArray[cATNUM]=0;
   weightArray[cATNUM]=0;

}


