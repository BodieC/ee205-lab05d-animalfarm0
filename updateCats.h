////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// Usage:  functions to update cat database
///
/// @file updateCats.h
///
/// @author Bodie Collins < bodie@hawaii.edu>
/// @date  22 Feb 2022
///////////////////////////////////////////////////////////////////////
#pragma once

extern int updateCatName(size_t catnUM, char* nAME);
extern int fixCat(size_t catNUM);
extern int updateCatWeight(size_t caTNUM, float weigHT);

