#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

const int  MAXNAMELENGTH=30;
#define MAXCATSINDATABASE 7

// name char array
char* nameArray[MAXCATSINDATABASE];
// enums of genders
enum gender {UNKNOWN_GENDER, MALE, FEMALE};
enum gender genderArray[MAXCATSINDATABASE];
// enums of breed
enum breed{UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};
enum breed breedArray[MAXCATSINDATABASE];
// is fixed bool
bool fixedArray[MAXCATSINDATABASE];
// weight float
float weightArray[MAXCATSINDATABASE];

// number of cats
size_t numofCats=0;

int addCats(char* namE,int gendeR, int breeD, bool fixeD, float weighT ) ;

int printAllCats();
int printCat(size_t catnuM);
int findCat(char* naME);

int updateCatName(size_t catnUM, char* nAME);
int fixCat(size_t catNUM);
int updateCatWeight(size_t caTNUM, float weigHT);

int addCats(char * namE, int gendeR, int breeD, bool fixeD, float weighT)
{  
   int error=0;

   if (numofCats>MAXCATSINDATABASE)
   {
     //if max cats is reached return error
      printf ("There is no more room in the database\n");
      error=1;
   }
   else if (strlen(namE) > MAXNAMELENGTH)
   {
      //if name is to long error
      printf ("The entered name is to long\n");
      error=1;
   }
   else if (strlen(namE)==0)
   {
      //if name is empty error
      printf ("You did not enter a name\n");
      error=1;
   }
   else if (weighT==0)
   {
      //if weight is equal to 0 error
      printf("Weight cannot equal 0\n");
      error=1;;
   }

   // tmp variable to go through array to check if same name
   size_t i=numofCats;

   //if it passes first set of validation check for same name
   if (error!=1)
   {
      while(i >  0)
      {
      
            // if the name is the same as the name before it
            if(namE == nameArray[i-1])
               {
         
               //print error
               printf ("The name you entered is already taken\n");
               error=1;
               break;
               }
            else if (namE != nameArray[i-1])
            {
               // iterate varible to check for next name
               #ifdef DEBUG               
               printf("current name %s vs name -1 %s\n", namE, nameArray[i-1]);
               #endif

               i--;
               continue;
            }
      
   
      }
      // if it passes same name check enter into database
      if (error != 1)
      { 
         //assign given parameters to arrays
         nameArray[numofCats+1]=namE;
         #ifdef DEBUG
         printf("numofCats=%lu\n",numofCats);
         #endif
         genderArray[numofCats+1]=gendeR;
         #ifdef DEBUG
         printf("numofCats=%lu\n",numofCats);
         #endif
         breedArray[numofCats+1]=breeD;
         #ifdef DEBUG
         printf("numofCats=%lu\n",numofCats);
         #endif
         fixedArray[numofCats+1]=fixeD;
         #ifdef DEBUG
         printf("numofCats=%lu\n",numofCats);
         #endif
         weightArray[numofCats+1]=weighT;
         #ifdef DEBUG
         printf("numofCats=%lu\n",numofCats);
         #endif

         //since it passed all validation and has been added to array change num of cats
         numofCats=numofCats+1;
         //printf("numofCats=%lu\n",numofCats);
         return numofCats-1;      
   
      }
   }

}


//FUNCTION TO PRINT ALL CATS
int printAllCats()
{
   //temp variable to cylce through printing names
   size_t ii=numofCats;
   while( ii > 0)
   {
      printf ("-----------------------------------");
      printf ("Cat number %lu information\n",ii);
      printf ("name= %s\n",nameArray[ii]);
      printf ("gender= %d\n", genderArray[ii]);
      printf ("breed=%d\n",breedArray[ii]);
      printf ("is fixed= %d\n",fixedArray[ii]);
      printf ("weight =%f\n",weightArray[ii]);

      ii=ii-1;

   }
return 1;
}

//FUNCTION TO PRINT CAT BY NUMBER IN ARRAY
int printCat(size_t catnuM)
{
   if (catnuM == 0 || catnuM > MAXCATSINDATABASE)
   {
      printf("animalFarm0:Badcat[%d]\n",catnuM);
   }
   else
   {
      printf ("-----------------------------------");
      printf ("Cat number %lu information\n",catnuM);
      printf ("name= %s\n",nameArray[catnuM]);
      printf ("gender= %d\n", genderArray[catnuM]);
      printf ("breed=%d\n",breedArray[catnuM]);
      printf ("is fixed= %d\n",fixedArray[catnuM]);
      printf ("weight =%f\n",weightArray[catnuM]);

   }
}

//FUNCTION TO FIND A CAT BY NAME
int findCat(char* naME)
{
   size_t iii=numofCats;
   while(iii>=0)
   {
      //if name matches cat number iii
      if (naME ==nameArray[iii])
         {
         printf ("-----------------------------------");
         printf ("Cat number %lu information\n",iii);
         printf ("name= %s\n",nameArray[iii]);
         printf ("gender= %d\n", genderArray[iii]);
         printf ("breed=%d\n",breedArray[iii]);
         printf ("is fixed= %d\n",fixedArray[iii]);
         printf ("weight =%f\n",weightArray[iii]);
         return iii;
         break;
         }
      //if cat name doesnt match iii decrement to check next name
      else if(naME !=nameArray[iii])
      { 
         iii=iii-1;
         
         // if its last name print error
         if (iii==0)
         {
            printf("ERROR:Cat Not Found.\n");
            break;
         }
         
         continue;
      }
               
   }
}

//FUNCTION TO UPDATE CAT NAME
int updateCatName(size_t catnUM, char* nAME)
{
   int erroR=0;
   //check if name is empty erroR=true if empty
   if (strlen(nAME)==0)
   {
      //if name is empty error
      printf ("ERROR: You did not enter a name\n");
      erroR=1;
   }

   else if (strlen(nAME) > MAXNAMELENGTH)
   {
      //if name is to long error
      printf ("ERROR:The entered name is to long\n");
      erroR=1;
   }

   //run through list of name change erroR to true if same name is already there
   size_t iiii=numofCats;
   while(iiii >  0)
      {

            // if the name is the same as the name before it
            if(nAME == nameArray[iiii-1])
               {

               //print error
               printf ("ERROR:The name you entered is already taken\n");
               erroR=1;
               break;
               }
            else if (nAME != nameArray[iiii-1])
            {
               // iterate varible to check for next name
               #ifdef DEBUG
               printf("current name %s vs name -1 %s\n", namE, nameArray[iiii-1]);
               #endif

               iiii--;
               continue;
            }
      }
   // if passes validation update name         
   if(erroR==0)
   {
   nameArray[catnUM]=nAME;
   }
}

//FUNCTION TO CHANGE IF A CAT IS FIXED
int fixCat(size_t catNUM)
{
   fixedArray[catNUM]=1;
}

//FUNCTION TO UPDATE CATS WEIGHT
int updateCatWeight(size_t caTNUM, float weigHT)
{
   if(weigHT>0)
   {
      weightArray[caTNUM]= weigHT;
   }
   else
   {
      printf("ERROR: Weight Cannot Equal or less than zero\n");
   }
}

//FUNCTION TO DELETE ALL CATS
int deleteAllCats()
{
   size_t foo=numofCats;
   
   while(foo>0)
   {
   nameArray[foo]="";
   genderArray[foo]=0;
   breedArray[foo]=0;
   fixedArray[foo]=0;
   weightArray[foo]=0;
   foo=foo-1;
   }
}

//DELETE ONE CAT
int deleteCat(size_t cATNUM)
{
   nameArray[cATNUM]="";
   genderArray[cATNUM]=0;
   breedArray[cATNUM]=0;
   fixedArray[cATNUM]=0;
   weightArray[cATNUM]=0;
   
}

int main()
{
printf("Starting Animal Farm 0\n");

addCats( "Loki", MALE, PERSIAN, true, 8.5 ) ;
addCats( "Milo", MALE, MANX, true, 30.0 ) ;
addCats( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
addCats( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
addCats( "Bell", FEMALE, MANX, true, 12.2 ) ;
addCats( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;
//printAllCats();
//printCat(0);
//printCat(2);
//printCat(6);
//printCat(0);
//printCat(35);
//findCat ("Milo");
//findCat ("Bell");
//findCat ("Dog");
//findCat("Loki");
//printCat(2);
//updateCatName(2 , "Bell");
//printCat(2);
//printf("Done with Animal Farm 0\n");

//printCat(4);
//fixCat(4);
//printCat(4);

//printCat(6);
//updateCatWeight(6,0);
//printCat(6);

//printAllCats();
//deleteAllCats();
//printAllCats();

printAllCats();
deleteCat(3);
printAllCats();

}
