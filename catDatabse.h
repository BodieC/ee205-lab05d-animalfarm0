///////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// @file catDatabse.h
///
///Database for Cats
///
//@author Bodie Collins <bodie@hawaii.edu>
/// @date 22 Feb 2022
/////////////////////////////////////////////////////////////////////////////
#pragma once
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

const size_t MAXNAMELENGTH=30;
#define MAXCATSINDATABASE 5

// name char array 
char* nameArray[MAXCATSINDATABASE];
// enums of genders
enum gender {UNKNOWN_GENDER, MALE, FEMALE};
enum gender genderArray[MAXCATSINDATABASE];
// enums of breed
enum breed{UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};
enum breed breedArray[MAXCATSINDATABASE];
// is fixed bool
bool fixedArray[MAXCATSINDATABASE];
// weight float
float weightArray[MAXCATSINDATABASE];
// number of cats
size_t numofCats=0;
