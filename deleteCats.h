////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// Usage:  functions to delete cats from database
///
/// @file deleteCats.h
///
/// @author Bodie Collins < bodie@hawaii.edu>
/// @date  22 Feb 2022
///////////////////////////////////////////////////////////////////////
#pragma once

extern int deleteAllCats();
extern int deleteCat(size_t cATNUM);


