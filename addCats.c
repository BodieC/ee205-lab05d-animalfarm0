////////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// @file addCats.c
/// @version 1.0
///
///
/// @author Bodie Collins < bodie@hawaii.edu>
/// @date  22 FEB 2022
///////////////////////////////////////////////////////////////////////
#include "catDatabse.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "addCats.h"

int addCats(char * namE, int gendeR, int breeD, bool fixeD, float weighT)
{
   int error=0;

   if (numofCats>MAXCATSINDATABASE)
   {
     //if max cats is reached return error
      printf ("There is no more room in the database\n");
      error=1;
   }
   else if (strlen(namE) > MAXNAMELENGTH)
   {
      //if name is to long error
      printf ("The entered name is to long\n");
      error=1;
   }
   else if (strlen(namE)==0)
   {
      //if name is empty error
      printf ("You did not enter a name\n");
      error=1;
   }
   else if (weighT==0)
   {
      //if weight is equal to 0 error
      printf("Weight cannot equal 0\n");
      error=1;;
   }

   // tmp variable to go through array to check if same name
   size_t i=numofCats;

   //if it passes first set of validation check for same name
   if (error!=1)
   {
      while(i >  0)
      {

            // if the name is the same as the name before it
            if(namE == nameArray[i-1])
               {

               //print error
               printf ("The name you entered is already taken\n");
               error=1;
               break;
               }
            else if (namE != nameArray[i-1])
            {
               // iterate varible to check for next name
               #ifdef DEBUG
               printf("current name %s vs name -1 %s\n", namE, nameArray[i-1]);
               #endif

               i--;
               continue;
            }


      }
      // if it passes same name check enter into database
      if (error != 1)
      {
         //assign given parameters to arrays
         nameArray[numofCats+1]=namE;
         #ifdef DEBUG
         printf("numofCats=%lu\n",numofCats);
         #endif
         genderArray[numofCats+1]=gendeR;
         #ifdef DEBUG
         printf("numofCats=%lu\n",numofCats);
         #endif
         breedArray[numofCats+1]=breeD;
         #ifdef DEBUG
         printf("numofCats=%lu\n",numofCats);
         #endif
         fixedArray[numofCats+1]=fixeD;
         #ifdef DEBUG
         printf("numofCats=%lu\n",numofCats);
         #endif
         weightArray[numofCats+1]=weighT;
         #ifdef DEBUG
         printf("numofCats=%lu\n",numofCats);
         #endif

         //since it passed all validation and has been added to array change num of cats
         numofCats=numofCats+1;
         //printf("numofCats=%lu\n",numofCats);
         return numofCats-1;

      }
   }
return 1;
}





