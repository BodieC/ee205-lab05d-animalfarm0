////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// Usage:  functions to update cat database
///
/// @file updateCats.c
///
/// @author Bodie Collins < bodie@hawaii.edu>
/// @date  22 Feb 2022
///////////////////////////////////////////////////////////////////////
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "catDatabse.h"
#include "updateCats.h"

//FUNCTION TO UPDATE CAT NAME
int updateCatName(size_t catnUM, char* nAME)
{
   int erroR=0;
   //check if name is empty erroR=true if empty
   if (strlen(nAME)==0)
   {
      //if name is empty error
      printf ("ERROR: You did not enter a name\n");
      erroR=1;
   }

   else if (strlen(nAME) > MAXNAMELENGTH)
   {
      //if name is to long error
      printf ("ERROR:The entered name is to long\n");
      erroR=1;
   }

   //run through list of name change erroR to true if same name is already there
   size_t iiii=numofCats;
   while(iiii >  0)
      {

            // if the name is the same as the name before it
            if(nAME == nameArray[iiii-1])
               {

               //print error
               printf ("ERROR:The name you entered is already taken\n");
               erroR=1;
               break;
               }
            else if (nAME != nameArray[iiii-1])
            {
               // iterate varible to check for next name
               #ifdef DEBUG
               printf("current name %s vs name -1 %s\n", namE, nameArray[iiii-1]);
               #endif

               iiii--;
               continue;
            }
      }
   // if passes validation update name
   if(erroR==0)
   {
   nameArray[catnUM]=nAME;
   }
}

//FUNCTION TO CHANGE IF A CAT IS FIXED
int fixCat(size_t catNUM)
{
   fixedArray[catNUM]=1;
}

//FUNCTION TO UPDATE CATS WEIGHT
int updateCatWeight(size_t caTNUM, float weigHT)
{
   if(weigHT>0)
   {
      weightArray[caTNUM]= weigHT;
   }
   else
   {
      printf("ERROR: Weight Cannot Equal or less than zero\n");
   }
}


