////////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// Usage:  print all of database, print single cat info, find a cat by name
///
/// @file reportCats.c
/// @version 1.0
///
/// @author Bodie Collins < bodie@hawaii.edu>
/// @date  22 Jan 2022
////////////////////////////////////////////////////////////////////////
#include "catDatabse.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "reportCats.h"

//FUNCTION TO PRINT ALL CATS
int printAllCats()
{
   //temp variable to cylce through printing names
   size_t ii=numofCats;
   while( ii > 0)
   {
      printf ("-----------------------------------");
      printf ("Cat number %lu information\n",ii);
      printf ("name= %s\n",nameArray[ii]);
      printf ("gender= %d\n", genderArray[ii]);
      printf ("breed=%d\n",breedArray[ii]);
      printf ("is fixed= %d\n",fixedArray[ii]);
      printf ("weight =%f\n",weightArray[ii]);

      ii=ii-1;

   }
return 1;
}

//FUNCTION TO PRINT CAT BY NUMBER IN ARRAY
int printCat(size_t catnuM)
{
   if (catnuM == 0 || catnuM > MAXCATSINDATABASE)
   {
      printf("animalFarm0:Badcat[%lu]\n",catnuM);
   }
   else
   {
      printf ("-----------------------------------");
      printf ("Cat number %lu information\n",catnuM);
      printf ("name= %s\n",nameArray[catnuM]);
      printf ("gender= %d\n", genderArray[catnuM]);
      printf ("breed=%d\n",breedArray[catnuM]);
      printf ("is fixed= %d\n",fixedArray[catnuM]);
      printf ("weight =%f\n",weightArray[catnuM]);

   }
}

//FUNCTION TO FIND A CAT BY NAME
int findCat(char* naME)
{
   size_t iii=numofCats;
   while(iii>=0)
   {
      //if name matches cat number iii
      if (naME ==nameArray[iii])
         {
         printf ("-----------------------------------");
         printf ("Cat number %lu information\n",iii);
         printf ("name= %s\n",nameArray[iii]);
         printf ("gender= %d\n", genderArray[iii]);
         printf ("breed=%d\n",breedArray[iii]);
         printf ("is fixed= %d\n",fixedArray[iii]);
         printf ("weight =%f\n",weightArray[iii]);
         return iii;
         break;
         }
      //if cat name doesnt match iii decrement to check next name
      else if(naME !=nameArray[iii])
      {
         iii=iii-1;

         // if its last name print error
         if (iii==0)
         {
            printf("ERROR:Cat Not Found.\n");
            break;
         }

         continue;
      }

   }
}

